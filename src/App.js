import React, { Component } from 'react';
import './App.css';
import Toolbar from './components/Toolbar';
import MessageList from './components/Message/List';

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {}
        this.state.messages = []

        this.delay = 0
    }

    componentDidMount() {
        this.retrieveMessages()
        .then(messages => {
            this.setState({messages: messages})
        })
    }

    async retrieveMessages() {
        return await fetch('/api/messages?delay=' + this.delay)
            .then(response => response.json())
            .then(responseData => responseData._embedded.messages)
    }

    async patchMessages(args) {
        return await fetch('/api/messages?delay=' + this.delay, {
                method: 'PATCH',
                body: JSON.stringify(args),
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                }
            })
    }

    async postMessage(subject, message) {
        return await fetch('/api/messages?delay=' + this.delay, {
                method: 'POST',
                body: JSON.stringify({'subject': subject, 'body': message}),
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                }
            })
    }

    updateMessage = (id, key, value) => {
        return this.updateSelectedMessage(key, value, [id])
    }

    updateSelectedMessage = (key, value, pre) => {
        var _selected = pre !== undefined ? pre : [],
            _patch = {
                    'messageIds': _selected,
                    'command': key,
                };

        if (value !== undefined) {
            _patch[key.includes('Label') ? 'label' : key] = value
        }

        if (!_selected.length) {
            this.state.messages.map((_item, _index) => {

                if (_item.selected) {
                    _selected.push(_item.id)
                }

                return _item
            });
        }

        return this.patchMessages(_patch)
            .then(response => {
                if (response.ok) {
                    this.retrieveMessages()
                        .then(messages => {

                            this.state.messages.map((_item, _index) => {
                                if (messages[_index] !== undefined && _item.id === messages[_index].id) {
                                    messages[_index].selected = _item.selected
                                    return _item
                                }

                                return false
                            });

                            this.setState((prevState) => ({'messages': messages}));
                        })
                }
            })
    }

    deleteSelected = (dataset) => {
        return this.updateSelectedMessage('delete')
    }

    updateAllMessages = (key, value) => {
        var _messages = this.state.messages

        _messages.map((_item, _index) => {
            _item[key] = value
            return _item
        });

        this.setState((prevState) => ({'messages': _messages}));
    }

    updateLabels = (value) => {
        this.updateSelectedMessage(value.action + 'Label', value.label)
    }

    localMessageUpdate = (id, key, value) => {
        var _messages = this.state.messages

        _messages.map((_item, _index) => {
            if (id === _item.id) {
                _item[key] = value
            }

            return _item
        });

        this.setState((prevState) => ({'messages': _messages}));
    }

    eventHandler = (dataset) => {
        if (dataset.event === 'selected') {
            return this.localMessageUpdate(dataset.message, dataset.event, dataset.value)
        }

        if (dataset.event && dataset.message !== undefined && dataset.value !== undefined) {
            return this.updateMessage(dataset.message, dataset.event, dataset.value);
        }

    }

    toolbarEventHandler = (dataset) => {

        if (dataset.event === 'delete') {
            return this.deleteSelected()
        }

        if (dataset.event === 'all') {
            return this.updateAllMessages(dataset.key, dataset.value)
        }

        if (dataset.event === 'label') {
            return this.updateLabels(dataset.value)
        }

        if (dataset.event === 'compose') {
            return this.postMessage(dataset.subject, dataset.body)
                .then(response => {
                    if (response.ok) {
                        this.retrieveMessages()
                            .then(messages => {

                                this.state.messages.map((_item, _index) => {
                                    messages[_index].selected = _item.selected
                                    return _item
                                });

                                this.setState((prevState) => ({'messages': messages}));
                            })
                    }
                })

        }

        if (dataset.event && dataset.value !== undefined) {
            return this.updateSelectedMessage(dataset.event, dataset.value);
        }

    }

    render() {
        return (
            <div className="App">
                <div className="navbar navbar-default" role="navigation">
                    <div className="container">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="/">React Inbox</a>
                        </div>
                    </div>
                </div>
                <Toolbar messages={ this.state.messages } handler={ this.toolbarEventHandler } />
                <MessageList messages={ this.state.messages } handler={ this.eventHandler } />
            </div>
        );
    }
}

export default App;
