import React from 'react';

class MessageItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            'show': false,
            'fetching': false,
            'body': null
        }

        this.props.message.selected = false
    }

    onSelect = (e) => {
        this.props.handler({
            'message': this.props.message.id,
            'event': 'selected',
            'value': !this.props.message.selected,
        })
    }

    async retrieveMessage() {
        return await fetch('/api/messages/' + this.props.message.id)
            .then(response => response.json())
            .then(responseData => responseData)

    }

    onShow = (e) => {
        this.setState({'show': !this.state.show})

        if (!this.state.show) {
            this.setState({'fetching': true})
            this.retrieveMessage().then(message => {
                this.setState({
                    'fetching': false,
                    'body': message.body
                })
                if (!this.props.message.read) {
                    this.props.handler({
                        'message': this.props.message.id,
                        'event': 'read',
                        'value': true,
                    })

                }
            })
        }

    }

    onStarred = (e) => {
        e.target.className = 'fa fa-spin fa-spinner'
        this.props.handler({
            'message': this.props.message.id,
            'event': 'star',
            'value': !this.props.message.starred,
        })
    }

    render() {
        return (
            <div>
                <div className={ 'row message ' + (this.props.message.read ? '' : 'un') + 'read' + (this.props.message.selected ? ' selected' : '') }>
                    <div className="col-xs-1">
                        <div className="row">
                            <div className="col-xs-2">
                                <input type="checkbox" onChange={ this.onSelect } checked={ this.props.message.selected } />
                            </div>
                            <div className="col-xs-2">
                                <i
                                    onClick={ this.onStarred }
                                    className={ 'star fa fa-star' + (this.props.message.starred ? '' : '-o') }></i>
                            </div>
                        </div>
                    </div>
                    <div className="col-xs-11 text-left">
                        { this.props.message.labels.map((label, i) => <span key={ i } className="label label-warning"> { label } </span> ) }
                        <a onClick={ this.onShow }>
                            { this.props.message.subject }
                        </a>
                    </div>
                </div>
                <div className={ 'row message-body' + (this.state.show ? '' : ' hide') } id={'message-body-' + this.props.message.id }>
                    <div className="col-xs-11 col-xs-offset-1">
                        { this.state.fetching
                            ? <span><i className="fa fa-spin fa-spinner"></i> Loading Message...</span>
                            : this.state.body }
                    </div>
                </div>
            </div>
        )
    }

}

export default MessageItem
