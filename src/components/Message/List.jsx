import React from 'react';
import MessageItem from './Item';

class MessageList extends React.Component {

    render() {
        var _load_style = {
            'fontSize': '172px',
            'color': '#999'
        }
        return (
            <div className="container">
                {
                    this.props.messages.length ?
                    this.props.messages.map(message => <MessageItem key={ message.id } message={ message } handler={ this.props.handler } />)
                    : <div className="text-center" style={ _load_style }><i className="fa fa-spin fa-spinner"></i></div>
                }
            </div>
        )
    }

}

export default MessageList
