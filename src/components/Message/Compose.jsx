import React from 'react';

class MessageCompose extends React.Component {

    constructor(props) {
        super(props)
        this.state = {'sending': false}
    }

    _post = (e) => {
        e.preventDefault()

        var _subject = this.refs.composeMessageSubject.value,
            _body = this.refs.composeMessageBody.value

        if (!_subject || !_body) {
            alert('Please enter both a message subject and a message body.')
            return
        }

        this.setState({'sending': true})

        this.props.handler({
            'event': 'compose',
            'subject': _subject,
            'body': _body
        })
        .then(response => {
            this.setState({'sending': false})
            this.refs.composeMessageSubject.value = '';
            this.refs.composeMessageBody.value = '';
            this.props.toggler();
        })

    }

    render() {
        return (
            <div className="container">
                <form className="form-horizontal well">
                    <div className="form-group">
                        <div className="col-sm-8 col-sm-offset-2">
                            <h4>Compose Message</h4>
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="subject" className="col-sm-2 control-label">Subject</label>
                        <div className="col-sm-8">
                            <input
                                ref="composeMessageSubject"
                                type="text"
                                className="form-control"
                                id="subject"
                                placeholder="Enter a subject"
                                name="subject" />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="body" className="col-sm-2 control-label">
                            Body
                        </label>
                        <div className="col-sm-8">
                            <textarea
                                ref="composeMessageBody"
                                name="body"
                                id="body"
                                className="form-control" />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-8 col-sm-offset-2">
                            <button
                                type="submit"
                                className="btn btn-primary"
                                onClick={ this._post }>
                                { this.state.sending ? <span><i className="fa fa-spin fa-spinner"></i>  SENDING</span> : 'SEND' }
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }

}

export default MessageCompose
