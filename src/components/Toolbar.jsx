import React from 'react';
import MessageCompose from './Message/Compose';

class Toolbar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
        this.state.showCompose = false
        this.state.selectAll = false
        this.state.marking_read = false
        this.state.marking_unread = false
        this.state.deleting = false
        this.state.selected_count = 0
    }

    _countUnread = (_messages) => {
        var _c = 0
        _messages.forEach((_item) => {
            if(!_item.read) {
                _c++
            }

        })

        return _c;
    }

    _countSelected = () => {
        var _c = 0
        this.props.messages.forEach((_item) => {
            if(_item.selected) {
                _c++
            }

        })

        return _c;
    }

    selectAllMessage = (e) => {
        this.props.handler({
            'event': 'all',
            'key': 'selected',
            'value': !this.state.selectAll
        })

        this.setState({'selectAll': !this.state.selectAll})
    }

    markRead = (e) => {
        var _state_key = 'marking_' + (e.target.dataset.read === 'true' ? 'read': 'unread'),
            _state = {}

        _state[_state_key] = true

        this.setState(_state)

        this.props.handler({
            'event': 'read',
            'value': e.target.dataset.read === 'true'
        })
        .then(response => {
            _state[_state_key] = false

            this.setState(_state)
        })
    }

    deleteSelected = (e) => {
        this.setState({'deleting': true})
        this.props.handler({
            'event': 'delete'
        })
        .then(response => {
            this.setState({'deleting': false})
        })
    }

    applyLabel = (e) => {
        if (e.target.options[e.target.selectedIndex].value !== 'false') {
            this.props.handler({
                'event': 'label',
                'value': {
                    'label': e.target.options[e.target.selectedIndex].value,
                    'action': e.target.dataset.action
                }
            })
        }
    }

    getSelectorClass = () => {
        var selected_count = this._countSelected(),
            total_count = this.props.messages.length

        if (!selected_count) {
            return ''
        }

        if (selected_count === total_count) {
            return 'check-'
        }

        return 'minus-';
    }

    showCompose = (e) => {
        this.setState({'showCompose': !this.state.showCompose})
    }

    render() {
        var _selectorClass = this.getSelectorClass()
        return (
            <div className="container">
                <div className="row toolbar">
                    <div className="col-md-12">
                        <p className="pull-right">
                            <span className="badge badge">{ this._countUnread(this.props.messages) }</span>
                            unread
                        </p>

                        <p className="pull-right">
                            <span className="badge badge">{ this._countSelected() }</span>
                            selected&nbsp;&nbsp;
                        </p>

                        <a className="btn btn-danger" onClick={ this.showCompose }>
                            <i className={'fa fa-' + (this.state.showCompose ? 'minus' : 'plus') }></i>
                        </a>

                        <button className="btn btn-default" onClick={ this.selectAllMessage }>
                            <i className={ 'fa fa-' + (_selectorClass) + 'square-o' }></i>
                        </button>

                        <button
                            className="btn btn-default"
                            onClick={ this.markRead }
                            data-read={ true }
                            disabled={ this._countSelected() ? '' : 'disabled' }>
                            { this.state.marking_read ? <span><i className="fa fa-spin fa-spinner"></i> Marking Read</span> : 'Mark As Read' }
                        </button>

                        <button
                            className="btn btn-default"
                            onClick={ this.markRead }
                            data-read={ false }
                            disabled={ this._countSelected() ? '' : 'disabled' }>
                            { this.state.marking_unread ? <span><i className="fa fa-spin fa-spinner"></i> Marking Unread</span> : 'Mark As Unread' }
                        </button>

                        <select
                            className="form-control label-select"
                            onChange={ this.applyLabel }
                            data-action="add"
                            disabled={ this._countSelected() ? '' : 'disabled' }>
                            <option value={ false }>Apply label</option>
                            <option value="dev">dev</option>
                            <option value="personal">personal</option>
                            <option value="gschool">gschool</option>
                        </select>

                        <select
                            className="form-control label-select"
                            onChange={ this.applyLabel }
                            data-action="remove"
                            disabled={ this._countSelected() ? '' : 'disabled' }>
                            <option value={ false }>Remove label</option>
                            <option value="dev">dev</option>
                            <option value="personal">personal</option>
                            <option value="gschool">gschool</option>
                        </select>

                        <button className="btn btn-default" onClick={ this.deleteSelected }>
                            <i className={ 'fa ' + (this.state.deleting ? 'fa-spin fa-spinner' : 'fa-trash-o') }></i>
                        </button>
                    </div>
                </div>
                { this.state.showCompose ? <MessageCompose toggler={ this.showCompose } handler={ this.props.handler } /> : '' }
            </div>
        )
    }

}

export default Toolbar
